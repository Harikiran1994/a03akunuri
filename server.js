var path = require("path");
var express = require("express");
var logger = require("morgan");
var bodyParser = require("body-parser");

var app = express();  // make express app
var http = require('http').Server(app);

// set up the view engine
app.set("views", path.resolve(__dirname, "views")); // path to views
app.set("view engine", "ejs"); // specify our view engine

// manage our entries
var entries = [];
app.locals.entries = entries;

// set up the logger
app.use(logger("dev"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname+'/assets/')));

app.get("/", function (request, response) {
  response.sendFile(path.join(__dirname+'/assets/index.html'));
});
app.get("/menu", function (request, response) {
  response.sendFile(path.join(__dirname+'/assets/Menupage.html'));
});
app.get("/about", function (request, response) {
  response.sendFile(path.join(__dirname+'/assets/Aboutpage.html'));
});
app.get("/contact", function (request, response) {
  response.sendFile(path.join(__dirname+'/assets/contactpage.html'));
});
app.post("/contact",function(req,response){

var api_key = 'key-0dd4047ccebbe0592af255863bde38c4';
var domain = 'sandbox7c051069d78d4d7a9f0281a8d83d0465.mailgun.org';
var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
 
var data = {
  from: req.body.fname+" "+req.body.lname+ ' <postmaster@sandbox7c051069d78d4d7a9f0281a8d83d0465.mailgun.org>',
  to: 'harikiran6040@gmail.com',
  subject: req.body.fname+" "+req.body.lname+" has sent you a message" ,
  html: "User with email id :"+req.body.email+" sent you a message <br/> Message: "+req.body.msg
};
 
mailgun.messages().send(data, function (error, body) {
  console.log(body);
  if(error)
  response.send("Mail Not Sent");
  else
  response.send("<script> alert('Mail Sent'); location.href='/'; </script>");
});
});

// http GET (default and /new-entry)
app.get("/guestbook", function (request, response) {
  response.render("guestbook");
});
app.get("/new-entry", function (request, response) {
  response.render("new-entry");
});


// http POST (INSERT)
app.post("/new-entry", function (request, response) {
  if (!request.body.title || !request.body.body) {
    response.status(400).send("Entries must have a title and a body.");
    return;
  }
  entries.push({
    title: request.body.title,
    content: request.body.body,
    published: new Date()
  });
  response.redirect("/guestbook");
});

// 404
app.use(function (request, response) {
  response.status(404).render("404");
});

// Listen for an application request on port 8081
http.listen(8081, function () {
  console.log('Guestbook app listening on http://127.0.0.1:8081/');
});
